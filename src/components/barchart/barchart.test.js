import React from 'react';
import { render } from '@testing-library/react';
import Barchart from './barchart';

test('renders barchart', () => {
  const { getByText } = render(<Barchart />);
  const linkElement = getByText(/barchart/i);
  expect(linkElement).toBeInTheDocument();
});
