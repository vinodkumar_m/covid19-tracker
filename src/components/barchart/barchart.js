import React, { Component } from 'react';
import './barchart.css';
import { getCovidStatistics } from '../../services/covidUserServices';
import Grid from '@material-ui/core/Grid';
import Chart from "react-apexcharts";
import Paper from '@material-ui/core/Paper';


class Barchart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chartStatus: false,
            options: {
                chart: {
                    id: "basic-bar"
                },
                xaxis: {
                    categories: []
                }
            },
            series: [
                {
                    name: "count",
                    data: []
                }
            ]
        };
    }
    async componentDidMount() {
        try {
            let response = await getCovidStatistics();
            let options = this.state.options;
            let series = this.state.series;
            if (response.data) {
                console.log("keys")
                response.data["confirmed"] && options.xaxis.categories.push("confirmed".toUpperCase());
                response.data["confirmed"] && series[0].data.push(response.data["confirmed"].value);
                response.data["recovered"] && options.xaxis.categories.push("recovered".toUpperCase());
                response.data["recovered"] && series[0].data.push(response.data["recovered"].value);
                response.data["deaths"] && options.xaxis.categories.push("deaths".toUpperCase());
                response.data["deaths"] && series[0].data.push(response.data["deaths"].value);
            }
            this.setState({ options: options, series: series, chartStatus: true });
        } catch (error) {
            console.log('some error occured', error);
        }
    }

    render() {
        return (
            <div>
                <Grid container direction="row" justify="center" alignItems="center">
                    <Grid item xs={12}>
                        <h1>COVID-19 Tracker</h1>
                    </Grid>
                    {this.state.chartStatus && <Grid container justify="center"
                        alignItems="center">
                        <Paper> <Chart
                            options={this.state.options}
                            series={this.state.series}
                            type="bar"
                            width="500"
                        />
                        </Paper>
                    </Grid>}
                </Grid>
            </div>
        );
    }
}
export default Barchart;
