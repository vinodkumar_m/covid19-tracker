const axios = require('axios');

export async function getRequest(url){
    try {
        const response = await axios.get(url);
        return response;
      } catch (error) {
        throw error;
      }
}

