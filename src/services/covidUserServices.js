import {apiURL} from '../config/dev';
import {getRequest} from  './axios.js';

// get covid statistics 
export function getCovidStatistics(){
   return getRequest(apiURL);
}