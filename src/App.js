import React from 'react';
import './App.css';
import Barchart from './components/barchart/barchart.js'

function App() {
  return (
    <div className="App">
        <Barchart />
    </div>
  );
}

export default App;

// #f5f6f9
// #292727